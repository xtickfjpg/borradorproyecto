

const fs = require('fs');

//****************************************
//* function escribir a fichero
//****************************************
function writeUserDataToFile(data) {

  console.log("writeUserDataToFile");
  // usuar la libreria fs

  // convertimos el array a un JSON en formato string
     var jsonUserData = JSON.stringify(data);
  //nombre del fichero, los datos del fichero, el formato
  // del enconding y que hacemos si hay error
     fs.writeFile(
       "./usuarios.json",
       jsonUserData,
       "utf8",
       function(err) {
          if (err) {
          console.log(err);
        } else {
          console.log("Datos de usuario escritos");
        }
       }
     )
}

module.exports.writeUserDataToFile = writeUserDataToFile;
