
// para importar el io.js donde hemos metido la function de escribir fichero
const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpg9ed/collections/";


//users?q={"$and": [{"id":{"$gt":15}},{"id":{"$lt":20}}]}&f={"first_name":1, "_id":0,"id":1, }&apiKey=PqXm3N6BAr82mMt3KCpx5R6tjg3UwXpP";

//******************************************
// API CONSULTA USUARIOS TOTAL Y CON FILTROS
//******************************************

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  // hay que cambiar la ruta del fichero..... hay que poner .. en vez de .
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);
}

//******************************************
// API CONSULTA USUARIOS TOTAL CON BD MONGO
//******************************************

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("users?" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
          var response =
          {
            "msg" :"error obteniendo usuario"
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              var response = body;
            } else {
              var response = {
                "msg" :"error obteniendo usuario"
              }
              res.status(404);
            }
          }
          res.send(response);
        }

  )
}


//******************************************
// API CONSULTA USUARIOS POR ID CON BD MONGO
//******************************************

function getUsersByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");
  var id = req.params.id;
  console.log("la id del usuario a consultar:" + id);
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
          var response =
          {
            "msg" :"error obteniendo usuario"
          }
          res.status(500);
        } else {
            if (body.length > 0) {
              var response = body[0];
            } else {
              var response = {
                "msg" :"error obteniendo usuario"
              }
              res.status(404);
            }
          }
          res.send(response);
    }
  )
}

//********************************
// API ALTA NUEVO USUARIO
//********************************

function createtUsersV1 (req, res) {
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);

  var users = require('../usuarios.json');

  var newUser = {};
  newUser.first_name = req.body.first_name;
  newUser.last_name = req.body.last_name;
  newUser.email = req.body.email;

  users.push(newUser); //añadimos un usuario al final del array
  io.writeUserDataToFile(users);
  console.log("usuario añadido");
}

//********************************
// API ALTA NUEVO USUARIO MONGO
//********************************

// EN MODIFICACION .....

function createtUsersV2 (req, res) {
  console.log("POST /apitechu/v2/users");
  console.log(req.body.first_name);
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);


  var newUser = {};
  newUser.id = req.body.id;
  newUser.first_name = req.body.first_name;
  newUser.last_name = req.body.last_name;
  newUser.email = req.body.email;
  newUser.password = crypt.hash(req.body.password);

  console.log("pw encriptado :" + newUser.password);

  var httpClient = requestJson.createClient(baseMlabURL);

  httpClient.post("users?" + mLabAPIKey, newUser ,
    function(err, resMLab, body) {
      console.log("usuario dado de alta correctamente");
      res.status(201).send({"msg": "usuario dado de alta correctamente"});
    }
  )
}

/*
{ url: "https://api.mlab.com/api/1/databases/my-db/collections/my-coll?apiKey=myAPIKey",
		  data: JSON.stringify( { "x" : 1 } ),
		  type: "POST",
		  contentType: "application/json" } );

  var newUser = {};
  newUser.first_name = req.body.first_name;
  newUser.last_name = req.body.last_name;
  newUser.email = req.body.email;



*/


//************************
// API BORRADO USUARIO
//************************
function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("el id a borrar es " + req.params.id);
  var users = require('../usuarios.json');

  for (var i = 0; i < users.length; i++) {
    console.log("bucle:"+ i);
    console.log("users[i]:"+ users[i].id);
     if (users[i].id == req.params.id ) {
       console.log("encuentro el elemento");
       console.log("usuario borrado." + "clave:"+ req.params.id + "elemento:" + i );
       console.log("first_name:" + users[i].first_name);
       console.log("last_name:" + users[i].last_name);
       console.log("email:" + users[i].email);
       users.splice(i,1);
       io.writeUserDataToFile(users);
       break;
     }
   }
 }


module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createtUsersV1 = createtUsersV1;
module.exports.createtUsersV2 = createtUsersV2;
module.exports.deleteUserV1 = deleteUserV1;
