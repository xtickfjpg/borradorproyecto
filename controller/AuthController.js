
// para importar el io.js donde hemos metido la function de escribir fichero
const io = require('../io');

const crypt = require('../crypt');
const requestJson = require('request-json');
const mLabAPIKey = "apiKey="+ process.env.MLAB_API_KEY;

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpg9ed/collections/";

//******************************************
// API REALIZAR EL LOGIN Y LOGOUT
//******************************************

function loginUsersV1 (req, res) {

  console.log("POST /apitechu/v1/login");
  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');
  var result = {};
  var encontrado = false;

  for (var i = 0; i < users.length; i++) {
    console.log("bucle:"+ i + "users[i]:" + users[i].email + "///" + users[i].password );
    if ((users[i].email == req.body.email) && (users[i].password == req.body.password)) {
       console.log("usuario password encontrado");
       console.log("email:" + users[i].email);
       users[i].logged = true;
       encontrado=true;
       io.writeUserDataToFile(users);
       break;
     }
   }

   if (encontrado) {
     result.mensaje = "Login correcto";
     result.idUsuario = users[i].id;
   } else {
     result.mensaje = "Login incorrecto";
   }
   res.send(result);
}

function logoutUsersV1 (req, res) {

  console.log("POST /apitechu/v1/logout");
  console.log(req.params.id);

  var users = require('../usuarios.json');
  var result = {};
  var encontrado = false;

  for (var i = 0; i < users.length; i++) {
    console.log(users[i].id + "///" + req.params.id );

    if ((users[i].id == req.params.id) && (users[i].logged===true)) {
       console.log("usuario encontrado y logado");
       console.log("id:" + users[i].id);
       delete users[i].logged;
       encontrado=true;
       io.writeUserDataToFile(users);
       break;
     }
   }


   if (encontrado) {
     result.mensaje = "logout correcto";
     result.idUsuario = users[i].id;
   } else {
     result.mensaje = "logout incorrecto";
   }
   res.send(result);
}


//******************************************
// API REALIZAR EL LOGIN Y LOGOUT MONGO
//******************************************

function loginUsersV2 (req, res) {

  console.log("POST /apitechu/v2/login");
  console.log(req.body.email);
  console.log(req.body.password);

  //recogemos las variables del
  var email=req.body.email;
  var password=req.body.password;

  var httpClient = requestJson.createClient(baseMlabURL);
  var putBody = '{"$set": {"logged":true}}';
  var query = 'q={"email":' + '"' + email + '"'+ '}';
  console.log(query);

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {
      console.log("dentro de la consulta de usuario");
      console.log(resMLabGET);
      if (errGET) {
          console.log("error 1 -GET");
          var response = {"msg" :"error al hacer loging"};
          res.status(500);
          res.send(response);
      } else {
            console.log("body recuperado : " + bodyGET.length);
            if (bodyGET.length == 0 ) {
            var response = {"msg" :"usuario no encontrado"};
            res.status(404);
            res.send(response);
          } else {
            console.log("por ok");
            console.log("password de entrada:" + req.body.password );
            console.log("password recuperada:" + bodyGET[0].password);
            console.log("llamada a bcrypt"  + crypt.checkPassword(req.body.password,bodyGET[0].password));
            if (crypt.checkPassword(req.body.password,bodyGET[0].password)) {
              console.log("usuario encontrado y pw ok");
              httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT) {
                if (errPUT) {
                  var response = {"msg" :"error login al hacer el put"};
                  res.status(500);
                } else {
                  console.log("LOGIN CORRECTO");
                  var response = {};
                  response.mensaje = "Login correcto";
                  response.idUsuario = bodyGET[0].id;
                  console.log("response:" + response.mensaje + response.idUsuario);
                  res.send(response);
                }
              }
            )
            } else {
              console.log("salgo por error de pw");
              var response = {};
              response.mensaje = "Login incorrecto";
              res.status(404);
              res.send(response);
            }
          }
      }
    }
  )
}

function logoutUsersV2 (req, res) {

  console.log("POST /apitechu/v2/logout");

  var httpClient = requestJson.createClient(baseMlabURL);
  var putBody = '{"$unset": {"logged":""}}';
  var query = 'q={"id":' + req.params.id + '}';
  console.log(query);

  httpClient.get("users?" + query + "&" + mLabAPIKey,
    function(errGET, resMLabGET, bodyGET) {
      if (errGET) {
        var response = {"msg" :"error peticion mlab al hacer el get del logout"};
        res.status(500);
        res.send(response);
      } else {
        if (bodyGET.length > 0 ) {
          console.log("id encontrado");
          httpClient.put("users?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
                console.log("logout CORRECTO");
                var response = {};
                response.mensaje = "Logout correcto";
                response.idUsuario = req.params.id;
                console.log("response:" + response.mensaje + response.idUsuario);
                res.send(response);
            }
          )
        } else {
          console.log("usuario no encontrado");
          var response = {"msg":"logout incorrecto"};
          res.status(404);
          res.send(response);
        }

      }

    }
  )
}

module.exports.loginUsersV1 = loginUsersV1;
module.exports.logoutUsersV1 = logoutUsersV1;
module.exports.loginUsersV2 = loginUsersV2;
module.exports.logoutUsersV2 = logoutUsersV2;
