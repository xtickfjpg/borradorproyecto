const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

// nuestro test hara una peticion a  www.duckduckgo.com'

describe('First test',
  function() {
    it('Test that DuckDuckgo works', function(done) {
        chai.request('http://www.duckduckgo.com')
        .get('/')
        .end(
          function(err, res) {
//          console.log(res);
            console.log("Request has finisihed");
            console.log(err);
            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }
)

describe('Test de API usuarios',
  function() {
    it('Prueba que la API usuarios responde correctamente', function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function(err, res) {
//          console.log(res);
            console.log("Request has finisihed");
            console.log(err);
            res.should.have.status(200);
            res.body.msg.should.be.eql("hola desde apitechu");
            done();
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos', function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err, res) {
            console.log("Request has finisihed");
            res.should.have.status(200);
            res.body.users.should.be.a('array'); // comprueba que sea un arrary
            for  (user of res.body.users) {
              user.should.have.property('email');
              user.should.have.property('password');
              user.should.have.property('first_name');
              
            }
            done();
          }
        )
      }
    )
  }
)
