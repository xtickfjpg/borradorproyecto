app.delete('/apitechu/v1/users/:id',
 function(req, res) {
   console.log("DELETE /apitechu/v1/users/:id");
   console.log("id es " + req.params.id);
   var users = require('./usuarios.json');
   var deleted = false;
   // console.log("Usando for normal");
   // for (var i = 0; i < users.length; i++) {
   //   console.log("comparando " + users[i].id + " y " +  req.params.id);
   //   if (users[i].id == req.params.id) {
   //     console.log("La posicion " + i + " coincide");
   //     users.splice(i, 1);
   //     deleted = true;
   //     break;
   //   }
   // }
   // console.log("Usando for in");
   // for (arrayId in users) {
   //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
   //   if (users[arrayId].id == req.params.id) {
   //     console.log("La posicion " + arrayId " coincide");
   //     users.splice(arrayId, 1);
   //     deleted = true;
   //     break;
   //   }
   // }
   // console.log("Usando for of");
   // for (user of users) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición ? coincide");
   //     // Which one to delete? order is not guaranteed...
   //     deleted = false;
   //     break;
   //   }
   // }
   // console.log("Usando for of 2");
   // // Destructuring, nodeJS v6+
   // for (var [index, user] of users.entries()) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posicion " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //     break;
   //   }
   // }
   // console.log("Usando for of 3");
   // var index = 0;
   // for (user of users) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //     break;
   //   }
   //   index++;
   // }
   // console.log("Usando Array ForEach");
   // users.forEach(function (user, index) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posicion " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //   }
   // });
   // console.log("Usando Array findIndex");
   // var indexOfElement = users.findIndex(
   //   function(element){
   //     console.log("comparando " + element.id + " y " +   req.params.id);
   //     return element.id == req.params.id
   //   }
   // )
   //
   // console.log("indexOfElement es " + indexOfElement);
   // if (indexOfElement >= 0) {
   //   users.splice(indexOfElement, 1);
   //   deleted = true;
   // }
// otra opcion con el findIndex con funcion externa
// console.log("Usando Array findIndex función a parte");
//   var indexOfElement = users.findIndex(testFindIndex, req);
// function testFindIndex(element) {
// console.log("comparando " + element.id + " y " + this.params.id);
// return this.params.id == element.id
//}


   if (deleted) {
     // io.writeUserDataToFile(users);
     writeUserDataToFile(users);
   }
   var msg = deleted ?
   "Usuario borrado" : "Usuario no encontrado."
   console.log(msg);
   res.send({"msg" : msg});
 }
)





const io = require('../io');
function loginV1(req, res) {
 console.log("POST /apitechu/v1/login");
 var users = require('../usuarios.json');
 for (user of users) {
   if (user.email == req.body.email
       && user.password == req.body.password) {
     console.log("Email found, password ok");
     var loggedUserId = user.id;
     user.logged = true;
     console.log("Logged in user with id " + user.id);
     io.writeUserDataToFile(users);
     break;
   }
 }
 var msg = loggedUserId ?
   "Login correcto" : "Login incorrecto, email y/o passsword no encontrados";
 var response = {
     "mensaje": msg,
     "idUsuario": loggedUserId
 };
 res.send(response);
}
function logoutV1(req, res) {
 console.log("POST /apitechu/v1/logout");
 var users = require('../usuarios.json');
 for (user of users) {
   if (user.id == req.params.id && user.logged === true) {
     console.log("User found, logging out");
     delete user.logged
     console.log("Logged out user with id " + user.id);
     var loggedoutUserId = user.id;
     io.writeUserDataToFile(users);
     break;
   }
 }
 var msg = loggedoutUserId ?
   "Logout correcto" : "Logout incorrecto";
 var response = {
     "mensaje": msg,
     "idUsuario": loggedoutUserId
 };
 res.send(response);
}
module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
